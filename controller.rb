# frozen_string_literal: true

# Main class for read and show stats
class Controller
  attr_accessor :teams

  def initialize
    puts 'Put filename bellow:'
    get_file gets.chomp
    show_stats
  end

  def get_file(user_input)
    if File.exist? user_input
      parse_results File.readlines(user_input)
    else
      puts "We can't find your file"
    end
  end

  def parse_results(lines)
    @teams = {}
    lines.each do |x|
      temp = x.split(', ')
      next unless temp.size == 2
      team_one = parse_team temp[0]
      team_two = parse_team temp[1]
      if team_one.any? && team_two.any?
        if team_one[-1] > team_two[-1]
          put_team team_one[0], 3
          put_team team_two[0], 0
        elsif team_one[-1] < team_two[-1]
          put_team team_one[0], 0
          put_team team_two[0], 3
        else
          put_team team_one[0], 1
          put_team team_two[0], 1
        end
      else
        puts "Troubles at line with #{x}"
      end
    end
  end

  def parse_team(temp_str)
    prepared_arr = []
    temp_arr = temp_str.split(' ')
    if temp_arr.size > 1
      prepared_arr.push temp_arr[0...-1].join(' ')
      prepared_arr.push temp_arr[-1].to_i
    end
    prepared_arr
  end

  def put_team(team, rank)
    if @teams.key? team
      @teams[team.to_s] += rank
    else
      hash = { team => rank }
      @teams.merge!(hash)
    end
  end

  def show_stats
    sorted_list = []
    @teams.each_pair { |key, value| sorted_list.push "#{value} #{key}" }
    sorted_list.sort!
    index = 0
    sorted_list.reverse.each { |value| puts "#{index += 1}. #{value}" }
  end
end

Controller.new
